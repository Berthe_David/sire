
#include "Raytracing.h"
#include "camera.h"

#include <Eigen/Geometry>
#include <QProgressDialog>

/** Render a scene using a raytracer.
  *
  */
QImage Raytracing::raytraceImage(Scene &scene)
{
    QProgressDialog progress("Raytracing...", "Cancel", 0, scene.camera().vpWidth() * scene.camera().vpHeight());
    progress.setWindowModality(Qt::WindowModal);

    using namespace Eigen;
    float tanfovy2 = tan(scene.camera().fovY()*0.5);
    Vector3f camX = scene.camera().right() * tanfovy2 * scene.camera().nearDist() * float(scene.camera().vpWidth())/float(scene.camera().vpHeight());
    Vector3f camY = scene.camera().up() * tanfovy2 * scene.camera().nearDist();
    Vector3f camF = scene.camera().direction() * scene.camera().nearDist();
    QImage img(scene.camera().vpWidth(), scene.camera().vpHeight(), QImage::Format_ARGB32);




Eigen::Array3f color;

    for(int j=0; j<scene.camera().vpHeight(); ++j)
        for(int i=0; i<scene.camera().vpWidth(); ++i)
        {
            progress.setValue(j*scene.camera().vpHeight() + i);
            if (progress.wasCanceled())
                return img;

            // compute the primary ray parameters
            Ray ray;
            ray.origin = scene.camera().position();
            scene.camera().direction();
            ray.direction = (camF + camX * (2.0*float(i+0.5)/float(scene.camera().vpWidth()) - 1.) - camY * (2.0*float(j+0.5)/float(scene.camera().vpHeight()) - 1.0)).normalized();

            Vector3f d = (camF + camX * (2.0*float(i+0.5)/float(scene.camera().vpWidth()) - 1.) - camY * (2.0*float(j+0.5)/float(scene.camera().vpHeight()) - 1.0));
            Vector3f dpp = (camF + camX * (2.0*float(i+1+0.5)/float(scene.camera().vpWidth()) - 1.) - camY * (2.0*float(j+0.5)/float(scene.camera().vpHeight()) - 1.0));
            Vector3f ray2 = ray.origin + dpp;
            Vector3f ray1 = ray.origin + d;
            Vector3f vect = ray2 - ray1;
            Vector3f diff = dpp - d;
//            std::cout << "pix : " << i << " " << j << std::endl;
//            std::cout << "vect : " << vect.x() << " " << vect.y() << " " << vect.z() << std::endl;
//            std::cout << "ray1 : " << ray1.x() << " " << ray1.y() << " " << ray1.z() << std::endl;
//            std::cout << "ray2 : " << ray2.x() << " " << ray2.y() << " " << ray2.z() << std::endl;
//            std::cout << "diff : " << diff.x() << " " << diff.y() << " " << diff.z() << std::endl;


            float xf, yf, zf;
            int xi, yi, zi;
            Vector3f direct;

            color = Array3f(0.,0.,0.);
            Ray rayon;


// Anti aliasing
//            int t = 10;
//            for(int p(0); p<t; ++p){
//                float ii = Eigen::internal::random(0., 1.);
//                float jj = Eigen::internal::random(0., 1.);

//                rayon.origin = ray.origin;
//                rayon.direction = (camF + camX * (2.0*float(i+ii+0.5)/float(scene.camera().vpWidth()) - 1.) - camY * (2.0*float(j+jj+0.5)/float(scene.camera().vpHeight()) - 1.0)).normalized();

//                color += scene.raytrace(rayon);
//            }

//            color /= t;






            // standard way (with aliasing)
            // raytrace the ray
            Eigen::Array3f color = scene.raytrace(ray);


//            std::cout << "color : " << color << std::endl;
            color /= color+0.25;

            // Basic tone mapping, and mapping from 0:1 to 0:255
            color = 255*Array3f(fmin(color(0),1.f),fmin(color(1),1.f),fmin(color(2),1.f));

            img.setPixel(i, j, qRgb(color(0), color(1), color(2)));
        }
    return img;
}
