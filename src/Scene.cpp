#include "Scene.h"
#include "Mesh.h"
#include "Sphere.h"
#include "Plane.h"
#include "DomUtils.h"

#include <Eigen/Geometry>

#include <QDomElement>
#include <QFile>

using namespace Eigen;

void Scene::draw() const
{
    for(int i=0; i<mObjectList.size(); ++i)
    {
        mObjectList[i]->draw();
    }
}

void Scene::clear()
{
    mObjectList.clear();
    mLightList.clear();
}

void Scene::createDefaultScene(Shader &Program)
{
    Object* pObj = 0;



    // create a sphere
    Sphere* pSphere1 = new Sphere(Eigen::Vector3f(0.0,0.0,0.0),0.1);
    // create an object
    pObj = new Object;
    pObj->attachShape(pSphere1);
    Affine3f M = Affine3f(Translation3f(0.6,-0.6,0.1));
    pObj->setTransformation(M.matrix());
    pObj->attachShader(&Program);
    // create a material
    BlinnPhong* ball_mat = new BlinnPhong(Array3f(0.3, 0.3, 0.8), Array3f(1, 1, 1), 256);
    Ward* boules_mat = new Ward(Array3f(0.f, 0.f, 0.f), Array3f(0.7f, 0.7f, 0.7f), 0.01f, 0.10f);
//    pObj->setMaterial(ball_mat);
    pObj->setMaterial(boules_mat);
    addObject(pObj);

    Sphere* pSphere2 = new Sphere(Eigen::Vector3f(0.0,0.0,0.0), 0.2);
    pObj = new Object;
    pObj->attachShape(pSphere2);
    M = Affine3f(Translation3f(-0.8,-0.8,0.2));
    pObj->setTransformation(M.matrix());
    pObj->attachShader(&Program);
    BlinnPhong* ball_mat2 = new BlinnPhong(Array3f(0.3, 0.8, 0.3), Array3f(0.04, 0.04, 0.04), 10);
//    pObj->setMaterial(ball_mat2);
    pObj->setMaterial(boules_mat);
    addObject(pObj);

    // create an infinite plane
    Plane* pPlane = new Plane();
    pObj = new Object;
    pObj->attachShape(pPlane);
    pObj->setTransformation(Affine3f::Identity().matrix());
    pObj->attachShader(&Program);
    BlinnPhong* floor_mat = new BlinnPhong(Array3f(0.7, 0.7, 0.7), Array3f(1, 1, 1), 30);
    pObj->setMaterial(floor_mat);
//    addObject(pObj);

    // create a mesh
    Mesh* pMesh = new Mesh(SIRE_DIR"/data/yoshi.off"); //less faces: tw503.off
    pMesh->makeUnitary();
    // uncomment to activate the creation of the BVH
    pMesh->buildBVH();
    pObj = new Object;
    pObj->attachShape(pMesh);
    pObj->attachShader(&Program);
    M = Translation3f(-0.4,0.4,0.5) * AngleAxisf(M_PI/4, Vector3f::UnitZ());
    pObj->setTransformation(M.matrix());
    BlinnPhong* tw_mat = new BlinnPhong(Array3f(0.4, 0.8, 0.4), Array3f(0, 0, 0), 0);
    pObj->setMaterial(tw_mat);
//    addObject(pObj);

    // setup the light sources
//    mLightList.push_back(new DirectionalLight(-Vector3f(1,1,1).normalized(), Array3f(0.6,0.6,0.6)));
//    mLightList.push_back(new PointLight(Vector3f(2,-5,5), Array3f(0.8,0.8,0.8), 20));


    // setup an arealight
//    mArealight(Vector3f(0,0,7), Vector3f(0,0,-4), 5.0, Array3f(0.9,0.9,0.9), 20.0);
    AreaLight* arealight = new AreaLight(Vector3f(-4,2,7), Vector3f(-4,2,-7).normalized(), 5.0, Array3f(1.9,1.9,1.9), 20.0);



//    arealight->loadTexture(SIRE_DIR"/data/light_source_1.png");


//    mLightList.push_back(arealight);


    // setup the camera
    mCamera.setViewport(512,512);
    mCamera.setFovY(M_PI/2.);
    mCamera.lookAt(Vector3f(1.2, -1.2, 1.2), Vector3f(0, 0, 0.1), Vector3f::UnitZ());

    // set background color
    mBackgroundColor = Array3f(0.2,0.2,0.2);

    mProgram = &Program;



    cubemap.load(SIRE_DIR"/data/grace_cross.hdr");

}

void Scene::addObject(Object* o)
{
    mObjectList.push_back(o);
}

void Scene::loadFromFile(const QString& filename)
{
    clear();

    QDomDocument doc(filename);
    QFile file(filename);
    if (!file.open(QIODevice::ReadOnly))
        return;
    if (!doc.setContent(&file))
    {
        file.close();
        return;
    }
    file.close();

    QDomElement docElem = doc.documentElement();

    mBackgroundColor = DomUtils::initColorFromDOMElement(docElem);

    QDomNode n = docElem.firstChild();
    while( !n.isNull() ) {
        QDomElement e = n.toElement();
        if( !e.isNull() ) {
            if (e.tagName() == "Sphere")
            {
                Sphere* pSphere = new Sphere(e);
                Object* pObj = new Object(e);
                pObj->attachShape(pSphere);
                pObj->attachShader(mProgram);
                addObject(pObj);
            }else if(e.tagName() == "Plane"){
                Plane* pPlane = new Plane();
                Object* pObj = new Object(e);
                pObj->attachShape(pPlane);
                pObj->attachShader(mProgram);
                addObject(pObj);
            }else if(e.tagName() == "Camera"){
                mCamera.initFromDomElement(e);
            }else if(e.tagName()=="DirectionalLight"){
                DirectionalLight* l = new DirectionalLight(e);
                mLightList.push_back(l);
            }else if(e.tagName()=="PointLight"){
                PointLight* l = new PointLight(e);
                mLightList.push_back(l);
            }else if(e.tagName()=="BackgroundColor"){
                mBackgroundColor = DomUtils::initColorFromDOMElement(e);
            }else{
                qWarning("Unsupported node : %s",qPrintable(e.tagName()));
            }
        }
        n = n.nextSibling();
    }
}

/** Search for the nearest intersection between the ray and the object list */
void Scene::intersect(const Ray& ray, Hit& hit)
{


    Eigen::Vector3f xx;
    for(int i=0; i<mObjectList.size(); ++i)
    {
        Ray local_ray;
        local_ray = ray;
        // apply transformation
        Eigen::Affine3f M(mObjectList[i]->transformation());
        Eigen::Affine3f invM = M.inverse();
        local_ray.origin = invM * ray.origin;
        local_ray.direction = invM.linear() * ray.direction;
        float old_t = hit.t();
        if(hit.foundIntersection())
        {
            // If previous intersection found, transform intersection point
            Eigen::Vector3f x = ray.at(hit.t());
            hit.setT( (invM * x - local_ray.origin).norm() );


            xx = ray.at(hit.t());

        }
        Hit h;
        mObjectList[i]->shape()->intersect(local_ray, h);

        if(h.t()<hit.t())
        {
            // we found a new closest intersection point for this object, record it:
            hit.setObject(mObjectList[i]);
            Eigen::Vector3f x = local_ray.at(h.t());
            hit.setNormal( (invM.linear().transpose() * h.normal()).normalized() );
            hit.setT( (M * x - ray.origin).norm() );
        }else{
            hit.setT(old_t);
        }

    }



}

/// recursively trace a ray, \returns the light intensity (as a RGB color) received at the origin of the ray in the direction of the ray
Eigen::Array3f Scene::raytrace(const Ray& ray)
{
    using namespace Eigen;
    Array3f value(0,0,0);

    // stopping criteria:
    if(ray.recursionLevel>=2 || ray.beta<0.1)
    {
        return value;
    }

    // find the first intersection point
    Hit hit;
    intersect(ray, hit);
    if(hit.foundIntersection())
    {
        Vector3f x = ray.at(hit.t());

        // add ambient
         value += 0.1 * hit.object()->material()->ambientColor();

        for(int i=0; i<mLightList.size(); ++i)
        {
            if(dynamic_cast<const AreaLight*>(mLightList[i]))
            {
               const AreaLight* light = dynamic_cast<const AreaLight*>(mLightList[i]);


               float taille = light->size();
               Eigen::Vector3f u = light->uVec();
               Eigen::Vector3f v = light->vVec();

               int nb_shadow = 0;


               Array3f value_tmp(.0,.0,.0);
               RayList.clear();



//            Début : échantillonage stratified jittered
               for(int ii(1); ii<taille; ++ii){
                for(int j(1); j<taille; ++j){

                    float ip = Eigen::internal::random(0., 1.);
                    float jp = Eigen::internal::random(0., 1.);

                    Eigen::Vector3f vjp = Eigen::Vector3f(jp,jp,jp);
                    Eigen::Vector3f vip = Eigen::Vector3f(ip,ip,ip);
//                    std::cout << "taille : " <<  taille << ii << j << std::endl;
//                   Eigen::Vector3f dir_light = light->position() - ray.origin + light->uVec()*i
                    Eigen::Vector3f lightPos = light->position() + (u*ii+vip)/taille + (v*j+vjp)/taille;
                    Eigen::Vector3f lightDir = -x + lightPos;
                    float dist = lightDir.norm();
                    lightDir.normalize();


                    Ray shadow_ray(x+hit.normal()*1e-4, lightDir);
                    shadow_ray.shadowRay = true;
                    Hit shadow_hit;
                    intersect(shadow_ray, shadow_hit);
                    if(shadow_hit.t()<dist)
                        continue;
//                        nb_shadow++;

                    float cos_term = std::max(0.f,lightDir.dot(hit.normal()));

                    value_tmp += cos_term * light->intensity(x, lightPos) * hit.object()->material()->brdf(-ray.direction, lightDir, hit.normal());

                }
               }

               value += value_tmp / (taille*taille);

            }
            else
            {

                // gestion par defaut...

                float dist;
                Vector3f lightDir = mLightList[i]->direction(x, &dist);
                Ray shadow_ray(x+hit.normal()*1e-4, lightDir);
                shadow_ray.shadowRay = true;
                Hit shadow_hit;
                intersect(shadow_ray, shadow_hit);
                if(shadow_hit.t()<dist)
                    continue;

                float cos_term = std::max(0.f,lightDir.dot(hit.normal()));
                value += cos_term * mLightList[i]->intensity(x) * hit.object()->material()->brdf(-ray.direction, lightDir, hit.normal());

            }
        }





//  value += /*cos_term * */cubemap.intensity(ray.direction) /** hit.object()->material()->brdf(-ray.direction, lightDir, hit.normal())*/;


        int N = 100;

        // reflexions
        {
            // compute the reflexion factor alpha
            Array3f alpha = hit.object()->material()->brdfReflect(-ray.direction, hit.normal());
//            Vector3f r = (ray.direction - 2.*ray.direction.dot(hit.normal())*hit.normal()).normalized();
//            Ray reflexion_ray(x+hit.normal()*1e-4, r);
//            reflexion_ray.recursionLevel = ray.recursionLevel + 1;
//            reflexion_ray.beta = ray.beta * alpha.matrix().norm();
//            value += alpha * raytrace(reflexion_ray);
            double xx, yy, zz;
            //srand(time(NULL));
            Vector3f x = ray.at(hit.t());

            Eigen::Array3f mc_brdf = Eigen::Array3f{0.f, 0.f, 0.f};
            int cpt = 0;
            for(int i(0); i<N; ++i){


                xx = (double)(rand()) / RAND_MAX * 2.0 - 1;
                yy = (double)(rand()) / RAND_MAX * 2.0 - 1;
                zz = (double)(rand()) / RAND_MAX * 2.0 - 1;

                Vector3f v = Vector3f(xx,yy,zz);

                while(v.norm() > 1){
                    xx = (double)(rand()) / RAND_MAX * 2.0 - 1;
                    yy = (double)(rand()) / RAND_MAX * 2.0 - 1;
                    zz = (double)(rand()) / RAND_MAX * 2.0 - 1;

                    v = Vector3f(xx,yy,zz);

                    if(v.norm() < 1)
                        v.normalize();
                }

                float doteuh = v.dot(hit.normal());
               // double angle = (double)acos(doteuh);

//                std::cout << doteuh << "      " << angle << std::endl;
                if(doteuh <  0){
                    v = -v;
                }
                    alpha = hit.object()->material()->brdf(-ray.direction.normalized(), v,hit.normal());

                    Ray reflexion_ray(x+hit.normal()*1e-4, v);
                    reflexion_ray.recursionLevel = ray.recursionLevel + 1;
                   // reflexion_ray.beta = ray.beta * alpha.matrix().norm();
                    float cos_term = std::max(0.f,v.dot(hit.normal()));
//                    float pdf;

//                    Ward* ward;

//                    int test;
//                    while((ward = const_cast<Ward*>(dynamic_cast<const Ward*>(hit.object()->material()))) == 0){
                        //Ward* ward = const_cast<Ward*>(dynamic_cast<const Ward*>(hit.object()->material()));
                       // pdf = ward->premultBrdf(-ray.direction);
//                        if(test != 0){
//  Partie 4 du td4
                           // Ward* ward = const_cast<Ward*>(dynamic_cast<const Ward*>(hit.object()->material()));
                           // mc_brdf += ward->premultBrdfquick(-ray.direction.normalized(), hit.normal()) * raytrace(reflexion_ray);
//                        }
//                     }
                        
                        
//                    }
// Partie 3 du td4
                    mc_brdf += ((cos_term * alpha)) * raytrace(reflexion_ray);
            }


            mc_brdf *= (2*M_PI);
            value += mc_brdf/(float)N;
        }



    }else /*if(ray.recursionLevel == 0)*/{
//        value = mBackgroundColor;
        value = cubemap.intensity(ray.direction);
    }

    return value;
}
