#ifndef SIRE_MATERIAL_H
#define SIRE_MATERIAL_H

#include <Eigen/Core>
#include <QDomElement>
#include <QImage>
#include <cmath>
#include <iostream>

class Material
{
public:

    virtual Eigen::Array3f ambientColor() const = 0;

    /// evaluate the BRDF
    virtual Eigen::Array3f brdf(const Eigen::Vector3f& viewDir, const Eigen::Vector3f& lightDir, const Eigen::Vector3f& normal) const = 0;

    /// evaluate the BRDF in the reflected direction
    virtual Eigen::Array3f brdfReflect(const Eigen::Vector3f& viewDir, const Eigen::Vector3f& normal) const = 0;

    /// texture
    enum TextureMode { MODULATE, BLEND, REPLACE };

    float textureScaleU() const { return m_textureScaleU; }
    float textureScaleV() const { return m_textureScaleV; }
    TextureMode textureMode() const { return m_textureMode; }
    const QImage& texture() const { return m_texture; }

    void setTexture(const QImage& texture) { m_texture = texture; }
    void loadTextureFromFile(const QString& fileName);
    void setTextureScale(float textureScale) { setTextureScaleU(textureScale); setTextureScaleV(textureScale); }
    void setTextureScaleU(float textureScaleU) { if (fabs(textureScaleU) > 1e-3) m_textureScaleU = textureScaleU; }
    void setTextureScaleV(float textureScaleV) { if (fabs(textureScaleV) > 1e-3) m_textureScaleV = textureScaleV; }
    void setTextureMode(TextureMode textureMode) { m_textureMode = textureMode; }

private:
    TextureMode m_textureMode;
    QImage m_texture;
    float m_textureScaleU, m_textureScaleV;
};


class BlinnPhong : public Material
{
public:

    BlinnPhong(const Eigen::Array3f& diffuseColor, const Eigen::Array3f& specularColor, float exponent)
        : m_diffuseColor(diffuseColor), m_specularColor(specularColor), m_exponent(exponent), m_reflectiveColor(specularColor)
    {}

    BlinnPhong(const QDomElement &e);

    Eigen::Array3f ambientColor() const
    {
        return m_diffuseColor;
    }

    Eigen::Array3f brdf(const Eigen::Vector3f& viewDir, const Eigen::Vector3f& lightDir, const Eigen::Vector3f& normal) const
    {
        Eigen::Vector3f h = (viewDir+lightDir).normalized();
        return m_diffuseColor + m_specularColor * pow(std::max(0.f,normal.dot(h)), m_exponent);
    }

    Eigen::Array3f brdfReflect(const Eigen::Vector3f& /*viewDir*/, const Eigen::Vector3f& /*normal*/) const
    {
        return m_reflectiveColor;
    }

protected:
    Eigen::Array3f m_diffuseColor;
    Eigen::Array3f m_specularColor;
    Eigen::Array3f m_reflectiveColor;
    float m_exponent;
};

class Ward : public Material
{
public:
    Ward(const Eigen::Array3f& diffuseColor, const Eigen::Array3f& specularColor, float alphax, float alphay)
        : m_diffuseColor(diffuseColor), m_specularColor(specularColor), m_reflectiveColor(specularColor), m_alphaX(alphax), m_alphaY(alphay), m_ps(0.75)
    {}


    Eigen::Array3f ambientColor() const
    {
        return m_diffuseColor;
    }



    Eigen::Array3f brdf(const Eigen::Vector3f& in, const Eigen::Vector3f& out, const Eigen::Vector3f& n) const
    {



        Eigen::Vector3f h = (in+out).normalized();

        Eigen::Vector3f up = Eigen::Vector3f(0.,0.,1.);
//        float cosi = fabsf(n.dot(up));

//        float angle = acos(cosi);
//        float rad90 = 180 * M_PI /180;
//        Eigen::Vector3f x = n * M_PI;

        Eigen::Vector3f x = (up - (n*(n.dot(up)))).normalized();
        //Eigen::Vector3f x = h * angle;
        Eigen::Vector3f y = ((x).cross(n)).normalized();



        float indotn = /*fabsf(*/in.dot(n);
        float outdotn = /*fabsf(*/std::max((double)(out.dot(n)), 1e-8);
        float hdotx = /*fabsf(*/h.dot(x);
        float hdoty = /*fabsf(*/h.dot(y);
        float hdotn = /*fabsf(*/h.dot(n);

        Eigen::Array3f res_ward = (m_specularColor/(std::max((double)(4*M_PI*m_alphaX*m_alphaY*sqrt((indotn) * (outdotn))), 1e-8))) *
                exp(-( ((pow(hdotx/m_alphaX, 2)) + (pow(hdoty/m_alphaY, 2))) / pow(hdotn, 2) ) );

        return m_diffuseColor + res_ward;
    }


    Eigen::Array3f brdfReflect(const Eigen::Vector3f& /*viewDir*/, const Eigen::Vector3f& /*normal*/) const
    {
        return m_reflectiveColor;
    }
//equation 9 du doc
    float premultBrdf(const Eigen::Vector3f& in){

        float u = (double)(rand()) / (double)RAND_MAX;
        float v = (double)(rand()) / (double)RAND_MAX;

        float phi = atan((m_alphaX/m_alphaY)*tan(2*M_PI*v));
        float theta = atan( sqrt( -log(u) / ( (cos(phi)*cos(phi))/(m_alphaX*m_alphaX) + (sin(phi)*sin(phi))/(m_alphaY*m_alphaY) ) ) );
        Eigen::Vector3f h = Eigen::Vector3f(sin(theta)*cos(phi), sin(theta)*sin(phi), cos(theta));
        Eigen::Vector3f o = 2*(in.normalized().dot(h.normalized()))*h.normalized()-in.normalized();

        float p = ( 1.f/( 4*M_PI*m_alphaX*m_alphaY*(h.normalized().dot(in.normalized()))*cos(theta)*cos(theta)*cos(theta) )) *
                exp( -(tan(theta) * tan(theta)) * ( (cos(phi) * cos(phi))/m_alphaX*m_alphaX  + (sin(phi) * sin(phi))/m_alphaY*m_alphaY )   );


        return p;
    }


//equation 10 du doc
    Eigen::Array3f premultBrdfquick(const Eigen::Vector3f& in, const Eigen::Vector3f& n){


        Eigen::Vector3f h = randRay(n);
        Eigen::Vector3f o = 2*(in.dot(h))*h-in;
        o.normalize();

//        std::cout << " div : " << o.dot(n)/in.dot(n) << std::endl;

        Eigen::Array3f res = /*m_diffuseColor + */m_specularColor * (h.dot(in)*h.dot(n)*h.dot(n)*h.dot(n)*sqrt(std::max(((-o).dot(n)/in.dot(n)), 0.f)));

        return res;
    }

		
    Eigen::Vector3f randRay(const Eigen::Vector3f& n){
        float u = (double)(rand()) / (double)RAND_MAX;
        float v = (double)(rand()) / (double)RAND_MAX;


        Eigen::Vector3f up = Eigen::Vector3f(0.,0.,1.);
        Eigen::Vector3f y = (up - (n*(n.dot(up)))).normalized();
        Eigen::Vector3f x = (y.cross(n)).normalized();


        float phi = atan((m_alphaY/m_alphaX)*tan(2.0*M_PI*v));
        float theta = atan( sqrt( (-log(u)) / ( (cos(phi)*cos(phi))/(m_alphaX*m_alphaX) + (sin(phi)*sin(phi))/(m_alphaY*m_alphaY) ) ) );

//        if(v>0.5)
//            phi += M_PI;

        Eigen::Vector3f h = (x * sin(theta)*cos(phi) + y * sin(theta)*sin(phi) + n * cos(theta)).normalized();
//        Vector3f o = 2*(dirnorm.dot(h.normalized()))*h.normalized()-dirnorm;

        return h.normalized();
    }

protected:
    Eigen::Array3f m_diffuseColor;
    Eigen::Array3f m_specularColor;
    Eigen::Array3f m_reflectiveColor;
    float m_alphaX;
    float m_alphaY;
    float m_ps;
};


#endif // SIRE_MATERIAL_H
