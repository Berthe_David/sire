
#ifndef SIRE_LIGHT_H
#define SIRE_LIGHT_H

#include <Eigen/Core>
#include <Eigen/Geometry>
#include <QMessageBox>
#include <QDomElement>

class Light
{
public:
    /** returns the direction (unit vector) from x to the light,
    * optionnaly, returns the distance to the light in *dist
    */
    virtual Eigen::Vector3f direction(const Eigen::Vector3f& x, float* dist = 0) const = 0;
    /// \returns the intensity emitted at x
    virtual Eigen::Array3f intensity(const Eigen::Vector3f& x) const = 0;

    Light(const Eigen::Array3f& a_intensity) : m_intensity(a_intensity) {}

    Light(const QDomElement& e);

protected:
    Eigen::Array3f m_intensity;
};

class DirectionalLight : public Light
{
public:
    DirectionalLight(const QDomElement& e);

    DirectionalLight(const Eigen::Vector3f& d, const Eigen::Array3f& a_intensity)
        : Light(a_intensity), m_direction(d) {}

    virtual Eigen::Vector3f direction(const Eigen::Vector3f& /*x*/, float* dist = 0) const
    {
        if(dist)
            *dist = std::numeric_limits<float>::max();
        return -m_direction;
    }
    virtual Eigen::Array3f intensity(const Eigen::Vector3f& x) const { return m_intensity; }
protected:
    Eigen::Vector3f m_direction;
};

class PointLight : public Light
{
public:
    PointLight(const QDomElement& e);
//    PointLight()
    /// \a radius is the influence radius
    PointLight(const Eigen::Vector3f& p, const Eigen::Array3f& a_intensity, float radius)
        : Light(a_intensity), m_position(p), m_radius(radius) {}

    virtual Eigen::Vector3f direction(const Eigen::Vector3f& x, float* dist = 0) const
    {
        Eigen::Vector3f diff = m_position-x;
        float norm = diff.norm();
        if(dist)
            *dist = norm;
        return diff/norm;
    }

    virtual Eigen::Array3f intensity(const Eigen::Vector3f& x) const
    {
        float d2 = (m_position-x).squaredNorm();
        float r2 = m_radius*m_radius;
        float w = 0;
        if(d2<r2)
        {
            w = 1.-d2/r2;
            w = w*w;
        }
        return w*m_intensity;
    }

protected:
    Eigen::Vector3f m_position;
    float m_radius;
};

class AreaLight : public PointLight
{
public:
    AreaLight(const Eigen::Vector3f& pos, const Eigen::Vector3f& dir, float a_size, const Eigen::Array3f& a_intensity, float radius)
        : PointLight(pos, a_intensity, radius), m_size(a_size)
    {
        m_frame.col(2) = -dir;
        m_frame.col(0) = m_frame.col(2).unitOrthogonal();
        m_frame.col(1) = m_frame.col(2).cross(m_frame.col(0));
    }

    // average intensity (to consider it as a point light)
    virtual Eigen::Array3f intensity(const Eigen::Vector3f& x) const
    {
        return std::max(0.f,(x-m_position).normalized().dot(direction())) * PointLight::intensity(x);
    }

    Eigen::Array3f intensity(const Eigen::Vector3f& x, const Eigen::Vector3f& y) const {
      // TODO
        float d2 = (x-y).squaredNorm();
        float r2 = m_radius*m_radius;
        float w = 0;
        if(d2<r2)
        {
            w = 1.-d2/r2;
            w = w*w;
        }
        return w*m_intensity;

//      return Eigen::Array3f::Constant(0.f);
    }

    void loadTexture(QString filename) {
        m_texture.load(filename);
    }

    //------------------------------------------------------------
    // Frame setters and getters
    /// sets the position of the camera
    void setPosition(const Eigen::Vector3f& pos) { m_position = pos; }
    /// \returns the position of the camera
    inline const Eigen::Vector3f& position() const { return m_position; }
    /// sets the orientation of the light
    void setOrientation(const Eigen::Quaternionf& q) { m_frame = q.toRotationMatrix();  }
    /// \returns the light direction, i.e., the -z axis of the frame
    Eigen::Vector3f direction() const { return -m_frame.col(2); }
    /// \returns the first tangent axis of the light plane
    Eigen::Vector3f uVec() const { return m_frame.col(1); }
    /// \returns the second tangent axis of the light plane
    Eigen::Vector3f vVec() const { return m_frame.col(0); }
    //------------------------------------------------------------

    float size() const { return m_size; }

protected:
    Eigen::Matrix3f m_frame;
    float m_size;
    QImage m_texture;
};

typedef std::vector<Light*> LightList;

#endif // SIRE_LIGHT_H
