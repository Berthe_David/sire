#version 150

in vec3 var_normal;
in vec3 var_view;

uniform vec3 light_dir;
uniform vec3 ambient_color;
out vec4 out_color;

void main()
{
    vec3 normal = normalize(var_normal);
    vec3 color  = ambient_color;
    vec3 view = normalize(var_view);
    vec3 h = normalize(view+light_dir);

    float diffuse = clamp(dot(normal,light_dir),0,1);
    float specular = 0;//pow(clamp(dot(normal,h),0,1),100);
    if(diffuse==0)
        specular = 0;
    out_color = vec4(vec3(0.02 + color*diffuse + specular), 1);
}
